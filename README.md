# Test Automation Framework

### Required software
[Full list is here](https://docs.microsoft.com/en-us/visualstudio/test/supported-configurations-and-platforms-for-coded-ui-tests-and-action-recordings?view=vs-2017)

Additional:

- installed Coded UI test component in VS
- Git VCS

### Setup project
- install MetaTrader 5 app to C:\Program Files\MetaTrader 5\
- set MetaTrader 5 interface language - English
- download project from git repo:
```cmd
cd /d D:\
mkdir Projects 
cd Projects
git clone https://roman_strizhak@bitbucket.org/roman_strizhak/mq-simple-taf.git
# OR
git clone git@bitbucket.org:roman_strizhak/mq-simple-taf.git
```

### Run tests in VS
1. open VS
2. load project solution => D:\Projects\mq-simple-taf\mq-simple-taf.sln		
3. build solution => menu Build - Build Solution
4. run tests

- menu Test - Run - All tests in Solution

OR

- right click on MetaTrader5 project at Solution Explorer - Run Unit Tests
		
### Run tests from cmd
```cmd
cd /d D:\Projects\mq-simple-taf
msbuild mq-simple-taf.sln // build project 
MSTest /testcontainer: MetaTrader5\bin\Debug\MetaTrader5.dll

# run autotests by category
MSTest /testcontainer: MetaTrader5\bin\Debug\MetaTrader5.dll /category:"E2E Suite"
```

### Reporting
User friendly report can be generated using [MSTestAllureAdapter](https://github.com/allure-framework/allure-mstest)