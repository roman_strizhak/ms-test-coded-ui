using MetaTrader5.Extensions;
using MetaTrader5.Windows;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace MetaTrader5.Steps
{
    public class OrdersActions
    {
        private readonly WinWindow orderDialog;

        public OrdersActions()
        {
            orderDialog = new MainAppWindow().GetSystemDialog("Order");
        }

        public void ClickButton(string buttonName)
        {
            orderDialog.GetByName<WinButton>(buttonName).Click();
        }

        public void SelectComboBoxItem(string itemName, int instance = 1)
        {
            orderDialog.Get<WinComboBox>(new {Instance = instance}).SelectedItem = itemName;
        }

        public void TypeValueIntoWinEditField(string fieldName, string value)
        {
            orderDialog.GetByName<WinEdit>(fieldName).CopyPastedText = value;
        }
    }
}