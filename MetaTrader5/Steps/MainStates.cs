using MetaTrader5.Extensions;
using MetaTrader5.Windows;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace MetaTrader5.Steps
{
    public class MainStates
    {
        private readonly MainAppWindow mainAppWindow;

        public MainStates()
        {
            mainAppWindow = new MainAppWindow();
        }

        public bool IsTerminalConnected()
        {
            return mainAppWindow.GetByClassName<WinToolBar>("ToolbarWindow32").GetByName<WinControl>("New Order").Enabled;
        }

        public bool AreThereOpenPositionsExistsInTradeGrid()
        {
            //TODO !mainAppWindow.isTradeGridEmpty
            return false;
        }

        public bool IsCreatedPositionDisplayed(string positionName)
        {
            // TODO get positions list => find position by positionName =>  is it exists
            return true;
        }
    }
}