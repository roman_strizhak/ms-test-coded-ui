using MetaTrader5.Extensions;
using MetaTrader5.Windows;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace MetaTrader5.Steps
{
    public class MainActions
    {
        private readonly MainAppWindow mainAppWindow;

        public MainActions()
        {
            mainAppWindow = new MainAppWindow();
        }

        public void SelectMenuItem(string parentMenuItemName, string childMenuItemName)
        {
            mainAppWindow.GetByName<WinMenuItem>(parentMenuItemName).Click();
            mainAppWindow.GetByName<WinMenuItem>(childMenuItemName).Click();
        }

        public void ClickButtonOnToolBar(string buttonName)
        {
            var mainMenuToolBar = mainAppWindow.GetByClassName<WinToolBar>("ToolbarWindow32");
            mainMenuToolBar.GetByName<WinControl>(buttonName).Click();
        }

        public void RemoveAllTradePositions()
        {
            //TODO get positions list => for each potition => rigth click on position => choose close action in context menu
        }

        public void CloseTradePosition(string ticketId)
        {
            //TODO get positions list => find position by ticketId => rigth click on position => choose close action in context menu
        }
    }
}