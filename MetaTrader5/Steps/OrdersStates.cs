using System;
using MetaTrader5.Extensions;
using MetaTrader5.Windows;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace MetaTrader5.Steps
{
    public class OrdersStates
    {
        private readonly WinWindow orderDialog;

        public OrdersStates()
        {
            orderDialog = new MainAppWindow().GetSystemDialog("Order");
        }

        public bool IsOrderDialogOpened()
        {
            return orderDialog.WaitForControlExist();
        }

        public bool IsOrderDialogClosed()
        {
            return orderDialog.WaitForControlNotExist();
        }

        public string GetOrderResultMesage()
        {
            orderDialog.GetByControlTypeAndClassName<WinControl>("Image", "Static").WaitForControlExist();
            string orderResult = orderDialog.GetByControlTypeAndClassName<WinControl>("Image", "Static").Name;
            Console.WriteLine(orderResult);
            return orderResult;
        }
    }
}