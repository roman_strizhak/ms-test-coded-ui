﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace MetaTrader5.Extensions
{
    public static class WinWindowExtensions
    {
        public static WinWindow AddTitle(this WinWindow control, string title)
        {
            if (!control.WindowTitles.Contains(title))
            {
                control.WindowTitles.Add(title);
            }
            return control;
        }

        public static WinWindow SetName(this WinWindow control, string name)
        {
            control.SearchProperties[UITestControl.PropertyNames.Name] = name;
            return control;
        }

        public static WinWindow SetClassName(this WinWindow control, string className, PropertyExpressionOperator conditionOperator = PropertyExpressionOperator.EqualTo)
        {
            control.SearchProperties[UITestControl.PropertyNames.ClassName] = className;
            return control;
        }
    }
}