﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace MetaTrader5.Extensions
{
    public static class UiTestControlExtensions
    {

        public static void Click(this UITestControl @this)
        {
            Mouse.Click(@this);
        }

        public static void RightClick(this UITestControl @this)
        {
            Mouse.Click(@this, MouseButtons.Right);
        }

        public static T GetByName<T>(this UITestControl @this, string name) where T : UITestControl, new()
        {
            var control = new T {Container = @this};
            control.SearchProperties[UITestControl.PropertyNames.Name] = name;
            @this.WindowTitles.ToList().ForEach(item => control.WindowTitles.Add(item));
            return control;
        }

        public static T GetByControlName<T>(this UITestControl @this, string controlName) where T : UITestControl, new()
        {
            var control = new T {Container = @this};
            control.SearchProperties[WinControl.PropertyNames.ControlName] = controlName;
            @this.WindowTitles.ToList().ForEach(item => control.WindowTitles.Add(item));
            return control;
        }

        public static T GetByClassName<T>(this UITestControl @this, string className) where T : UITestControl, new()
        {
            var control = new T {Container = @this};
            control.SearchProperties.Add(UITestControl.PropertyNames.ClassName, className, PropertyExpressionOperator.Contains);
            @this.WindowTitles.ToList().ForEach(item => control.WindowTitles.Add(item));
            return control;
        }

        public static T GetByControlTypeAndClassName<T>(this UITestControl @this, string controlType, string className) where T : UITestControl, new()
        {
            var control = new T { Container = @this };
            control.SearchProperties[UITestControl.PropertyNames.ControlType] = controlType;
            control.SearchProperties.Add(UITestControl.PropertyNames.ClassName, className, PropertyExpressionOperator.Contains);
            @this.WindowTitles.ToList().ForEach(item => control.WindowTitles.Add(item));
            return control;
        }

        public static T Get<T>(this UITestControl @this, dynamic searchProperties, dynamic filterProperties = null) where T : UITestControl, new()
        {
            var control = new T {Container = @this};
            IEnumerable<string> propertyNames = ((object) searchProperties).GetPropertiesForObject();
            foreach (string item in propertyNames)
            {
                control.SearchProperties.Add(item, ((object) searchProperties).GetPropertyValue(item).ToString());
            }

            if (filterProperties != null)
            {
                propertyNames = ((object) filterProperties).GetPropertiesForObject();
                foreach (string item in propertyNames)
                {
                    control.SearchProperties.Add(item, ((object) filterProperties).GetPropertyValue(item).ToString());
                }
            }

            return control;
        }

        private static IEnumerable<string> GetPropertiesForObject(this object @this)
        {
            return (from propertyInfo in @this.GetType().GetProperties() select propertyInfo.Name).ToList();
        }

        private static object GetPropertyValue(this object @this, string propertyName)
        {
            object result = null;

            PropertyInfo propertyInfo = (from info in @this.GetType().GetProperties() where info.Name == propertyName select info).FirstOrDefault();
            if (propertyInfo != null)
            {
                result = propertyInfo.GetValue(@this, null);
            }

            return result;
        }
    }
}