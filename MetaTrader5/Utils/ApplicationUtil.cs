﻿using System;
using System.Diagnostics;
using MetaTrader5.Core;

namespace MetaTrader5.Utils
{
    public class ApplicationUtil
    {
        public static void RunMetaTrader()
        {
            CloseMetaTrader();
            Process.Start(AppConfig.MetaTraderAppPath);
            Console.WriteLine("MetaTrader is started");
        }

        public static void RunMetaTraderWithConfig(string configPath)
        {
            CloseMetaTrader();
            string mtArguments = string.Format("/config:{0}", configPath);
            var processStartInfo = new ProcessStartInfo
            {
                FileName = AppConfig.MetaTraderAppPath,
                Arguments = mtArguments
            };
            Process.Start(processStartInfo);
            Console.WriteLine("MetaTrader is started with config file: " + configPath);
        }

        public static void CloseMetaTrader()
        {
            foreach (Process mtProcess in Process.GetProcessesByName(AppConfig.MetaTraderProcessName))
            {
                mtProcess.Kill();
                Console.WriteLine("MetaTrader process is closed");
            }
        }
    }
}