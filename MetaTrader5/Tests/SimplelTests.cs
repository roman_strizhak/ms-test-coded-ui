using MetaTrader5.Steps;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MetaTrader5.Tests
{
    [CodedUITest]
    public class SimplelTests : AbstractBaseTest
    {
        private readonly MainActions mainActions = new MainActions();
        private readonly MainStates mainStates = new MainStates();
        private readonly OrdersActions ordersActions = new OrdersActions();
        private readonly OrdersStates ordersStates = new OrdersStates();

        [TestMethod]
        [Owner("roman.strizhak@gmail.com")]
        [TestCategory("Smoke Suite")]
        public void InitialTerminalState()
        {
            Assert.IsTrue(mainStates.IsTerminalConnected());
            Assert.IsFalse(mainStates.AreThereOpenPositionsExistsInTradeGrid());
        }

        [TestMethod]
        [TestCategory("Smoke Suite")]
        public void CreateSellInstantExecutionOrder()
        {
            // given
            mainActions.RemoveAllTradePositions();
            // when
            mainActions.ClickButtonOnToolBar("New Order");
            Assert.IsTrue(ordersStates.IsOrderDialogOpened(), "New Order dialog wasn't opened");

            #region fill new order fields

            ordersActions.SelectComboBoxItem("Instant Execution", 2); // select type
            ordersActions.TypeValueIntoWinEditField("Volume:", "1.23");
            ordersActions.TypeValueIntoWinEditField("Stop Loss:", "0.99999");
            ordersActions.TypeValueIntoWinEditField("Take Profit:", "0.77777");
            ordersActions.TypeValueIntoWinEditField("Comment:", "TestInstantExecutionOrder");
            ordersActions.SelectComboBoxItem("5", 3); // select deviation

            #endregion

            ordersActions.ClickButton("Sell");
//            Assert.IsTrue(ordersStates.GetOrderResultMesage().Contains("Done"), "Order wasn't created");

//            ordersActions.ClickButton("OK");
            Assert.IsTrue(ordersStates.IsOrderDialogClosed(), "New Order dialog wasn't closed");
            // then
            Assert.IsTrue(mainStates.IsCreatedPositionDisplayed("ticketId"));

            mainActions.SelectMenuItem("File", "Exit");
        }

        [TestMethod]
        [TestCategory("Regression Suite")]
        public void CreatePendingOrder()
        {
            // given
            mainActions.RemoveAllTradePositions();
            // when
            mainActions.ClickButtonOnToolBar("New Order");
            Assert.IsTrue(ordersStates.IsOrderDialogOpened(), "New Order dialog wasn't opened");

            #region fill new order fields

            ordersActions.SelectComboBoxItem("Pending Order", 2); // select type
            ordersActions.TypeValueIntoWinEditField("Volume:", "0.50");
            ordersActions.TypeValueIntoWinEditField("Price:", "0.99000");
            ordersActions.SelectComboBoxItem("Today", 4); // select expiration
            ordersActions.TypeValueIntoWinEditField("Comment:", "TestPendingOrder");

            #endregion

            ordersActions.ClickButton("Place");
            Assert.IsTrue(ordersStates.IsOrderDialogClosed(), "New Order dialog wasn't closed");
            // then
            Assert.IsTrue(mainStates.IsCreatedPositionDisplayed("ticketId"));

            mainActions.SelectMenuItem("File", "Exit");
        }

        [TestMethod]
        [TestCategory("Not implemented")]
        public void CloseTradePosition()
        {
            // given
            mainActions.RemoveAllTradePositions(); // step isn't implemeted

            #region create new order

            mainActions.ClickButtonOnToolBar("New Order");
            Assert.IsTrue(ordersStates.IsOrderDialogOpened(), "New Order dialog wasn't opened");

            ordersActions.SelectComboBoxItem("Instant Execution", 2); // select type
            ordersActions.TypeValueIntoWinEditField("Volume:", "2.22");
            ordersActions.TypeValueIntoWinEditField("Comment:", "Buy order");
            ordersActions.SelectComboBoxItem("3", 3); // select deviation

            ordersActions.ClickButton("Buy");
//            Assert.IsTrue(ordersStates.GetOrderResultMesage().Contains("Done"), "Order wasn't created");

//            ordersActions.ClickButton("OK");
            Assert.IsTrue(ordersStates.IsOrderDialogClosed(), "New Order dialog wasn't closed");

            #endregion

            // when
            mainActions.CloseTradePosition("ticketId"); // step isn't implemeted
            // then
            Assert.IsFalse(mainStates.AreThereOpenPositionsExistsInTradeGrid()); // step isn't implemeted

            mainActions.SelectMenuItem("File", "Exit");
        }

        [TestMethod]
        [TestCategory("Failed Suite")]
        public void FailedTestExampleForReport()
        {
            Assert.AreEqual("foo", "bar", "Values are different");
        }
    }
}