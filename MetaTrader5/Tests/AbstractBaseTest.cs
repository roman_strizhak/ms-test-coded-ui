﻿using MetaTrader5.Core;
using MetaTrader5.Utils;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MetaTrader5.Tests
{
    [CodedUITest]
    public abstract class AbstractBaseTest
    {
        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        protected TestContext TestContext { get; set; }

        #region  Test conditions

        [TestInitialize] //Use TestInitialize to run code before running each test
        public void TestPreConditions()
        {
//            ApplicationUtil.RunMetaTrader();
            ApplicationUtil.RunMetaTraderWithConfig(AppConfig.MetaTraderTestConfigPath);
            Playback.Wait(2000);
        }


        [TestCleanup] //Use TestCleanup to run code after each test has run
        public void TestPostConditions()
        {
            ApplicationUtil.CloseMetaTrader();
        }

        #endregion
    }
}