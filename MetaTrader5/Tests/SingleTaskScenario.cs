using MetaTrader5.Steps;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MetaTrader5.Tests
{
    [CodedUITest]
    public class SingleTaskScenario : AbstractBaseTest
    {
        private readonly MainActions mainActions = new MainActions();
        private readonly MainStates mainStates = new MainStates();
        private readonly OrdersActions ordersActions = new OrdersActions();
        private readonly OrdersStates ordersStates = new OrdersStates();

        [TestMethod]
        [TestCategory("E2E Suite")]
        public void CommonTaskScenario()
        {
            Assert.IsTrue(mainStates.IsTerminalConnected());
            Assert.IsFalse(mainStates.AreThereOpenPositionsExistsInTradeGrid()); // step isn't implemeted

            #region Create New Order

            mainActions.ClickButtonOnToolBar("New Order");
            Assert.IsTrue(ordersStates.IsOrderDialogOpened(), "New Order dialog wasn't opened");
            ordersActions.SelectComboBoxItem("Instant Execution", 2); // select type
            ordersActions.TypeValueIntoWinEditField("Volume:", "1.01");
            ordersActions.TypeValueIntoWinEditField("Comment:", "CommonTaskScenario");
            ordersActions.SelectComboBoxItem("1", 3); // select deviation
            ordersActions.ClickButton("Sell");
//            Assert.IsTrue(ordersStates.GetOrderResultMesage().Contains("Done"), "Order wasn't created");

//            ordersActions.ClickButton("OK");
            Assert.IsTrue(ordersStates.IsOrderDialogClosed(), "New Order dialog wasn't closed");

            #endregion

            mainActions.CloseTradePosition("ticketId"); // step isn't implemeted

            Assert.IsFalse(mainStates.AreThereOpenPositionsExistsInTradeGrid()); // step isn't implemeted

            mainActions.SelectMenuItem("File", "Exit");
        }
    }
}