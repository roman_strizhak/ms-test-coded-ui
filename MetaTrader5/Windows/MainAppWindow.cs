using MetaTrader5.Extensions;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace MetaTrader5.Windows
{
    public class MainAppWindow : WinWindow
    {
        public MainAppWindow()
        {
            SearchProperties.Add(new PropertyExpression(UITestControl.PropertyNames.ClassName, "MetaQuotes::MetaTrader::5.00", PropertyExpressionOperator.Contains));
        }

        public WinWindow GetSystemDialog(string title)
        {
            return new WinWindow().SetClassName("#32770", PropertyExpressionOperator.Contains).SetName(title).AddTitle(title);
        }
    }
}