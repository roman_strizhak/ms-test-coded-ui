using System;
using System.Configuration;

namespace MetaTrader5.Core
{
    public static class AppConfig
    {
        public static string ProjectPath
        {
            get { return ConfigurationManager.AppSettings["ProjectPath"]; }
        }

        public static string MetaTraderAppPath
        {
            get { return ConfigurationManager.AppSettings["MetaTraderAppPath"]; }
        }

        public static string MetaTraderProcessName

        {
            get { return ConfigurationManager.AppSettings["MetaTraderProcessName"]; }
        }

        public static string MetaTraderTestConfigPath
        {
            get { return ConfigurationManager.AppSettings["MetaTraderTestConfigPath"]; }
        }

        public static int SearchTimeout
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["SearchTimeout"]); }
        }

        public static int WaitForReadyTimeout
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["WaitForReadyTimeout"]); }
        }
    }
}